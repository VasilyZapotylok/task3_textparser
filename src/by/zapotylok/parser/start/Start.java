package by.zapotylok.parser.start;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import org.apache.log4j.xml.DOMConfigurator;

import by.zapotylok.parser.composite.StringComposite;
import by.zapotylok.parser.composite.StringType;
import by.zapotylok.parser.parser.TextParser;
import by.zapotylok.parser.util.FileReaderService;
import by.zapotylok.parser.util.FileWriterService;
import by.zapotylok.parser.util.ReplaceUtil;
import by.zapotylok.parser.util.SortUtil;

public class Start {
	static {
		new DOMConfigurator().doConfigure("resources/log4j.xml", LogManager.getLoggerRepository());
	}

	static final Logger LOGGER = Logger.getLogger(Start.class);

	public static void main(String[] args) {

		try {
			TextParser parser = new TextParser();
			String textForReading = "files/textIn.txt";
			String text = FileReaderService.readFile(textForReading);
			StringComposite composite = new StringComposite(StringType.PARAGRAPH_AND_LISTING);
			parser.parseText(composite, text);
			LOGGER.info("Parsing done");
			String textAfterParsing = "files/textOut.txt";
			FileWriterService.writeFile(composite.buildText(), textAfterParsing);
			LOGGER.info("Writing to file done");
			String sort = SortUtil.sortSentences(composite);
			String sentencesAfterSort = "files/sentencesSort.txt";
			FileWriterService.writeFile(sort, sentencesAfterSort);
			LOGGER.info("Sentences were sorted by quantity of words");
			ReplaceUtil.replaceFirstAndLastWords(composite);
			String textAfterReplace = "files/textReplace.txt";
			FileWriterService.writeFile(composite.buildText(), textAfterReplace);
			LOGGER.info("First and last words in sentences were changed");
		} catch (RuntimeException e) {
			LOGGER.error("File for reading is missing. Work is impossible.");
		}
	}
}
