package by.zapotylok.parser.util;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import org.apache.log4j.Logger;

public class FileReaderService {
	static final Logger LOGGER = Logger.getLogger(FileReaderService.class);

	public static String readFile(String path) {
		StringBuilder stringBuilder = new StringBuilder();
		try (Scanner scanner = new Scanner(new File(path))) {
			while (scanner.hasNext()) {
				stringBuilder.append(scanner.nextLine() + "\n");
			}
		} catch (IOException e) {
			LOGGER.error("File access problem in reading");
			throw new RuntimeException();
		}
		return stringBuilder.toString();
	}
}
