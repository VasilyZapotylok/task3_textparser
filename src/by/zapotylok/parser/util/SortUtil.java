package by.zapotylok.parser.util;

import java.util.ArrayList;
import java.util.List;

import by.zapotylok.parser.composite.StringComponent;
import by.zapotylok.parser.composite.StringType;

public class SortUtil {
	public static String sortSentences(StringComponent component) {
		List<StringComponent> sentences = new ArrayList<>();
		CreatelSentences(component, sentences);
		sentences.sort(
				(StringComponent s1, StringComponent s2) -> s1.getComponents().size() - s2.getComponents().size());
		StringBuilder stringBuilder = new StringBuilder();
		for (StringComponent sentence : sentences) {
			String temp = sentence.buildText();
			stringBuilder.append(temp + "\n");

		}
		return stringBuilder.toString();

	}

	public static void CreatelSentences(StringComponent component, List<StringComponent> sentences) {
		if (component.getType() != StringType.LISTING) {
			for (StringComponent childComponent : component.getComponents()) {
				if (childComponent.getType() == StringType.SENTENCE) {
					sentences.add(childComponent);
				} else {
					CreatelSentences(childComponent, sentences);
				}
			}
		}
	}
}
