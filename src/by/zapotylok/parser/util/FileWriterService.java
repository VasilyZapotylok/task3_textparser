package by.zapotylok.parser.util;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import org.apache.log4j.Logger;

public class FileWriterService {
	static final Logger LOGGER = Logger.getLogger(FileWriterService.class);

	public static void writeFile(String text, String path) {
		try (BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path)))) {
			bufferedWriter.write(text);
		} catch (IOException e) {
			LOGGER.error("File access problem in writing");
		}
	}

}
