package by.zapotylok.parser.util;

import java.util.ArrayList;
import java.util.List;

import by.zapotylok.parser.composite.StringComponent;
import by.zapotylok.parser.composite.StringType;

public class ReplaceUtil {
	public static void replaceFirstAndLastWords(StringComponent component) {
		List<StringComponent> sentences = new ArrayList<>();
		CreatelSentences(component, sentences);
		for (StringComponent sentence : sentences) {
			StringComponent firstLexeme = sentence.getComponents().get(0);
			StringComponent lastLexeme = sentence.getComponents().get(sentence.getComponents().size() - 1);
			String firstWord = firstLexeme.getComponents().get(0).buildText();
			String lastWord = lastLexeme.getComponents().get(0).buildText();
			firstLexeme.getComponents().get(0).setText(lastWord);
			lastLexeme.getComponents().get(0).setText(firstWord);
		}
	}

	public static void CreatelSentences(StringComponent component, List<StringComponent> sentences) {
		if (component.getType() != StringType.LISTING) {
			for (StringComponent childComponent : component.getComponents()) {
				if (childComponent.getType() == StringType.SENTENCE) {
					sentences.add(childComponent);
				} else {
					CreatelSentences(childComponent, sentences);
				}
			}
		}
	}
}
