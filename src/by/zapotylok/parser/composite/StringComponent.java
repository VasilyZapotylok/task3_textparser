package by.zapotylok.parser.composite;

import java.util.List;

public interface StringComponent {

	void add(StringComponent component);

	void setText(String text);

	List<StringComponent> getComponents();

	String buildText();

	StringType getType();

}
