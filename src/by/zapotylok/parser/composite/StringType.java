package by.zapotylok.parser.composite;

public enum StringType {
	
	
	PARAGRAPH_AND_LISTING("(([\\u0020]{6})([\\w\\u0020\\p{Punct}\\n]+)(\\.\\t))(\\n)*(\\/\\/:[^А-Я]+\\/\\/\\/:~)*"), 
	LISTING ("\\/\\/:[^А-Я]+\\/\\/\\/:~"),
	PARAGRAPH ("([\\u0020]{6})([\\w\\u0020\\p{Punct}\\n]+)(\\.\\t)"),
	SENTENCE("([A-Z])([\\w\\u0020,()\\n]+)[.!?][\\s]"), 
	LEXEME("[\\w\\p{Punct}]+[\\s]"),  
	WORD("[\\wA-Za-z]+"), 
	SYMBOL("[\\p{Punct}]"),
	SPACE("\\s");
	
	private String regexp;

	private StringType(String regexp) {
		this.regexp = regexp;
	}

	public String getRegexp() {
		return regexp;
	}

}
