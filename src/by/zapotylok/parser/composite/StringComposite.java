package by.zapotylok.parser.composite;

import java.util.ArrayList;
import java.util.List;

public class StringComposite implements StringComponent {
	private ArrayList<StringComponent> components;
	private StringType type;

	public StringComposite(StringType type) {
		components = new ArrayList<>();
		this.type = type;
	}

	@Override
	public String buildText() {
		StringBuilder sb = new StringBuilder();
		for (StringComponent component : components) {
			sb.append(component.buildText());
		}
		return sb.toString();
	}

	@Override
	public void add(StringComponent component) {
		components.add(component);
	}

	@Override
	public List<StringComponent> getComponents() {
		return components;
	}

	@Override
	public StringType getType() {
		return type;
	}

	public void setType(StringType type) {
		this.type = type;
	}

	@Override
	public void setText(String text) {

	}

}
