package by.zapotylok.parser.composite;

import java.util.List;

public class StringLeaf implements StringComponent {
	private String text;
	private StringType type;

	public StringLeaf(String text, StringType type) {
		this.text = text;
		this.type = type;
	}

	@Override
	public String buildText() {
		return text;
	}

	@Override
	public void add(StringComponent component) {

	}

	@Override
	public List<StringComponent> getComponents() {
		throw new UnsupportedOperationException();
	}

	@Override
	public StringType getType() {
		return type;
	}

	public void setType(StringType type) {
		this.type = type;
	}

	@Override
	public void setText(String text) {
		this.text = text;
	}
}
