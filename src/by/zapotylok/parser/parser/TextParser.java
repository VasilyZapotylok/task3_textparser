package by.zapotylok.parser.parser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import by.zapotylok.parser.composite.StringComposite;
import by.zapotylok.parser.composite.StringLeaf;
import by.zapotylok.parser.composite.StringType;

public class TextParser {
	public void parseText(StringComposite composite, String text) {
		parseToParagraphAndListing(composite, text);
	}

	private void parseToParagraphAndListing(StringComposite composite, String text) {
		Pattern pattern = Pattern.compile(StringType.PARAGRAPH_AND_LISTING.getRegexp());
		Matcher matcher = pattern.matcher(text);
		while (matcher.find()) {
			StringComposite childComposite = new StringComposite(StringType.PARAGRAPH_AND_LISTING);
			composite.add(childComposite);
			parseToParagraph(childComposite, matcher.group());
			parseToLeaf(childComposite, StringType.LISTING, matcher.group());
		}
	}

	private void parseToParagraph(StringComposite composite, String text) {
		Pattern pattern = Pattern.compile(StringType.PARAGRAPH.getRegexp());
		Matcher matcher = pattern.matcher(text);
		while (matcher.find()) {
			StringComposite childComposite = new StringComposite(StringType.PARAGRAPH);
			composite.add(childComposite);
			parseToSentence(childComposite, matcher.group());
		}
	}

	private void parseToSentence(StringComposite composite, String text) {
		Pattern pattern = Pattern.compile(StringType.SENTENCE.getRegexp());
		Matcher matcher = pattern.matcher(text);
		while (matcher.find()) {
			StringComposite childComposite = new StringComposite(StringType.SENTENCE);
			composite.add(childComposite);
			parseToLexeme(childComposite, matcher.group());
		}
	}

	private void parseToLexeme(StringComposite composite, String text) {
		Pattern pattern = Pattern.compile(StringType.LEXEME.getRegexp());
		Matcher matcher = pattern.matcher(text);
		while (matcher.find()) {
			StringComposite childComposite = new StringComposite(StringType.LEXEME);
			composite.add(childComposite);
			parseToLeaf(childComposite, StringType.WORD, matcher.group());
			parseToLeaf(childComposite, StringType.SYMBOL, matcher.group());
			parseToLeaf(childComposite, StringType.SPACE, matcher.group());

		}
	}

	private void parseToLeaf(StringComposite composite, StringType type, String text) {
		Pattern pattern = Pattern.compile(type.getRegexp());
		Matcher matcher = pattern.matcher(text);
		while (matcher.find()) {
			StringLeaf childComposite = new StringLeaf(matcher.group(), type);
			composite.add(childComposite);
		}
	}
}
